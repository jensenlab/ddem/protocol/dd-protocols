import Basic from "components/ddx/Basic.tmpl_20AD45E7.jsx";
import Sample from "components/ddx/Samples.tmpl_636E68FA.jsx";
import Image from "components/ddx/Image.tmpl_1AC73C98.jsx";
import Protocol from "components/ddx/Protocol.tmpl_2EF80254.jsx";

export { Basic, Image, Sample, Protocol };
