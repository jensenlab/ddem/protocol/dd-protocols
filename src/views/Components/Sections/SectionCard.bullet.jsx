import React from "react";

import { Link } from "react-router-dom";

// @material-ui/core components
import withStyles from "@material-ui/core/styles/withStyles";

// core components
import cardStyle from "assets/jss/material-kit-react/views/componentsSections/cardStyle.jsx";
import {
  Box,
  Card,
  CardActionArea,
  CardMedia,
  Typography
} from "@material-ui/core";

import { OipApi } from "oip/OipApi";
import { config } from "ddx.config.js";

const api = new OipApi(config.daemonApiUrl);

class SectionCard extends React.Component {
  state = {
    name: "",
    description: "",
    address: "",
    designedBy: [],
    txid: ""
  };

  componentDidMount() {
    if (this.props.data) {
      console.log(this.props.data);
      const recordInfo = this.props.data.record.details;
      if (recordInfo) {
        const designedByIds =
          recordInfo[config.protocolHandler.by.tmpl][
            config.protocolHandler.by.name
          ];
        console.log(designedByIds);
        let name =
          recordInfo[config.cardInfo.name.tmpl][config.cardInfo.name.name];

        const description =
          recordInfo[config.cardInfo.description.tmpl][
            config.cardInfo.description.name
          ];

        const txid = this.props.data.meta.txid;

        this.setState({
          name,
          description,
          txid
        });

        if (designedByIds) {
          designedByIds.forEach(person => {
            const callPeople = api.getRecord(person);
            callPeople.then(response => {
              console.log(response);
              if (response !== "not found") {
                const name =
                  response.results[0].record.details[config.cardInfo.name.tmpl][
                    config.cardInfo.name.name
                  ];
                const surname =
                  response.results[0].record.details[
                    config.cardInfo.surname.tmpl
                  ][config.cardInfo.surname.name];
                const fullName = `${name} ${surname}`;
                this.state.designedBy.push(fullName);
                this.setState({ designedBy: this.state.designedBy });
              }
            });
          });
        }
      }
    }
  }

  render() {
    const { classes } = this.props;
    return (
      <Card className={classes.bullet}>
        <Box display="flex" flexDirection="column" flexGrow={1} height="100%">
          <Box display="flex" flexGrow={1}>
            <Link
              className={classes.cardLink}
              to={"/record/" + this.state.txid}
            >
              <CardActionArea>
                <Box p={1} display="flex" flexGrow={1} width="100%">
                  <Box pr={1} flexDirection="column" flexGrow={1}>
                    <Typography
                      // noWrap={true}
                      variant="h5"
                      // style={{ fontStyle: "bold" }}
                      color="textPrimary"
                    >
                      {this.state.name}
                    </Typography>
                    <Typography
                      // noWrap={true}
                      variant="caption"
                      style={{ overflowWrap: "break-word" }}
                      color="textPrimary"
                    >
                      {this.state.description}
                    </Typography>
                  </Box>
                </Box>
              </CardActionArea>
            </Link>
          </Box>

          <Box
            p={1}
            display="flex"
            flexDirection="column"
            flexShrink={1}
            alignItems="flex-end"
          >
            <Box p={1} alignContent="right">
              <Typography
                // noWrap={true}
                variant="caption"
                style={{ overflowWrap: "break-word" }}
                color="textPrimary"
              >
                {this.state.designedBy.join(", ")}
              </Typography>
            </Box>
            <Box display="inline" maxWidth="100%">
              <Typography
                style={{
                  display: "block",
                  fontSize: "8px",
                  overflowWrap: "break-word",
                  fontStyle: "italic",
                  fontFamily: "courier"
                }}
                color="textPrimary"
              >
                {this.state.txid}
              </Typography>
            </Box>
          </Box>
        </Box>
      </Card>
    );
  }
}

export default withStyles(cardStyle, { withTheme: true })(SectionCard);
