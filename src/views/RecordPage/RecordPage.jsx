import React from "react";
// nodejs library that concatenates classes
import classNames from "classnames";
// @material-ui/core components
import withStyles from "@material-ui/core/styles/withStyles";
import Link from "@material-ui/core/Link";
import Typography from "@material-ui/core/Typography";
// @material-ui/icons

// core components
import Header from "components/Header/Header.jsx";
import Footer from "components/Footer/Footer.jsx";
import GridContainer from "components/Grid/GridContainer.jsx";
import GridItem from "components/Grid/GridItem.jsx";
import HeaderLinks from "components/Header/HeaderLinks.jsx";
import Parallax from "components/Parallax/Parallax.jsx";
import recordPageStyle from "assets/jss/material-kit-react/views/recordPage.jsx";
import { Box, Grid } from "@material-ui/core";

import { OipApi } from "oip/OipApi";
import { config } from "ddx.config.js";

import SnackbarContent from "components/Snackbar/SnackbarContent.jsx";
import { Snackbar } from "@material-ui/core";

const api = new OipApi(config.daemonApiUrl);
const dashboardRoutes = [];

class RecordPage extends React.Component {
  constructor(data) {
    super();
    this.oipref = data.match.params.id;
    this.state = {
      name: "",
      description: "",
      imgRef: require("assets/img/ddx-placeHolder.png"),
      by: [],
      materials: [],
      devices: [],
      software: [],
      others: [],
      notes: [],
      organizations: [],
      protocols: [],
      legacyIds: [],
      steps: [],
      error: false,
      errorMessage: "we cannot find parts of this record."
    };
  }

  checkData(id, data) {
    if (data.results.length === 0) {
      return this.setState({ error: true }, () => {
        return false;
      });
    }
    return true;
  }

  basicHandlerOfOipRef = id => {
    console.log(`Receiving: ${id}`);
    return api.getRecord(id).then(data => {
      console.log(`Data received: ${JSON.stringify(data)}`);
      if (!this.checkData(id, data)) {
        return {
          id,
          name: ""
        };
      }
      const name =
        data.results[0].record.details[config.cardInfo.name.tmpl][
          config.cardInfo.name.name
        ];
      return {
        id,
        name
      };
    });
  };

  peopleHandlerOfOipRef = id => {
    console.log(`Receiving: ${id}`);
    return api.getRecord(id).then(data => {
      console.log(`Data received: ${data}`);
      if (!this.checkData(id, data)) {
        return {
          id,
          name: ""
        };
      }
      let name =
        data.results[0].record.details[config.cardInfo.name.tmpl][
          config.cardInfo.name.name
        ];

      if (data.results[0].record.details[config.cardInfo.surname.tmpl]) {
        name +=
          " " +
          data.results[0].record.details[config.cardInfo.surname.tmpl][
            config.cardInfo.surname.name
          ];
      }
      return {
        id,
        name
      };
    });
  };

  handleOipRefs = (type, ids, handler) => {
    const items = ids.map(handler);

    Promise.all(items).then(names => {
      this.setState({
        [type]: names
      });
    });
  };

  componentDidMount() {
    api.getRecord(this.oipref).then(data => {
      if (!this.checkData(this.oipref, data)) {
        return {
          id: this.oipref,
          name: ""
        };
      }
      const recordInfo = data.results[0].record.details;
      if (recordInfo) {
        console.log(recordInfo)
        const avatarId =
          recordInfo[config.cardInfo.avatarRecord.tmpl][
            config.cardInfo.avatarRecord.name
          ];

        if (avatarId) {
          console.log(`avatarId: ${avatarId}`);
          const callAvatar = api.getRecord(avatarId);
          callAvatar.then(avatar => {
            const address =
              avatar.results[0].record.details[config.imageHandler.main.tmpl][
                config.imageHandler.main.name
              ];
            this.setState({
              imgRef: `${config.ipfs.apiUrl}${address}`
            });
          });
        }

        const name =
          recordInfo[config.cardInfo.name.tmpl][config.cardInfo.name.name];

        const description =
          recordInfo[config.cardInfo.description.tmpl][
            config.cardInfo.description.name
          ];

        const steps =
          recordInfo[config.protocolHandler.steps.tmpl][
            config.protocolHandler.steps.name
          ] || [];

        const others =
          recordInfo[config.protocolHandler.others.tmpl][
            config.protocolHandler.others.name
          ] || [];

        const byOipRef =
          recordInfo[config.protocolHandler.by.tmpl][
            config.protocolHandler.by.name
          ];

        if (byOipRef) {
          this.handleOipRefs("by", byOipRef, this.peopleHandlerOfOipRef);
        }

        const organizationsOipRef =
          recordInfo[config.protocolHandler.organizations.tmpl][
            config.protocolHandler.organizations.name
          ];

        if (organizationsOipRef) {
          this.handleOipRefs(
            "organizations",
            organizationsOipRef,
            this.basicHandlerOfOipRef
          );
        }

        const materialsOipRef =
          recordInfo[config.protocolHandler.materials.tmpl][
            config.protocolHandler.materials.name
          ];

        if (materialsOipRef) {
          this.handleOipRefs(
            "materials",
            materialsOipRef,
            this.basicHandlerOfOipRef
          );
        }

        const devicesOipRef =
          recordInfo[config.protocolHandler.devices.tmpl][
            config.protocolHandler.devices.name
          ];

        if (devicesOipRef) {
          this.handleOipRefs(
            "devices",
            devicesOipRef,
            this.basicHandlerOfOipRef
          );
        }

        const softwareOipRef =
          recordInfo[config.protocolHandler.software.tmpl][
            config.protocolHandler.software.name
          ];

        if (softwareOipRef) {
          this.handleOipRefs(
            "software",
            softwareOipRef,
            this.basicHandlerOfOipRef
          );
        }

        const protocolsOipRef =
          recordInfo[config.protocolHandler.protocols.tmpl][
            config.protocolHandler.protocols.name
          ];

        if (protocolsOipRef) {
          this.handleOipRefs(
            "protocols",
            protocolsOipRef,
            this.peopleHandlerOfOipRef
          );
        }

        const legacyIds =
          recordInfo[config.protocolHandler.legacyIds.tmpl][
            config.protocolHandler.legacyIds.name
          ] || [];

        /*         if (legacyIdsOipRef) {
          const legacyIds = legacyIdsOipRef.map(this.basicHandlerOfOipRef);

          Promise.all(legacyIds).then(names => {
            this.setState({
              legacyIds: names
            });
          });
        } */

        /*         if (preparedFromOipRef) {
          const preparedFrom = preparedFromOipRef.map(
            this.basicHandlerOfOipRef
          );

          Promise.all(preparedFrom).then(names => {
            this.setState({
              preparedFrom: names
            });
          });
        } */

        this.setState({
          name,
          description,
          steps,
          others,
          legacyIds
        });
      }
    });
  }

  render() {
    console.log(this.oipref);
    const { classes, ...rest } = this.props;
    return (
      <div>
        <Header
          color="transparent"
          routes={dashboardRoutes}
          brand={config.title}
          rightLinks={<HeaderLinks />}
          fixed
          changeColorOnScroll={{
            height: 400,
            color: "white"
          }}
          {...rest}
        />
        <Parallax className={classes.small}>
          <div className={classes.container}>
            <GridContainer>
              <GridItem xs={12} sm={12} md={6}>
                <h1 className={classes.title}>{this.state.name}</h1>
              </GridItem>
            </GridContainer>
          </div>
        </Parallax>
        <Snackbar
          open={this.state.error}
          autoHideDuration={6000}
          style={{ width: "100%" }}
        >
          <SnackbarContent
            message={
              <span>
                <b>Warning</b> {this.state.errorMessage}
              </span>
            }
            color="danger"
            close
            icon="info_outline"
          />
        </Snackbar>
        <div className={classNames(classes.main, classes.mainRaised)}>
          <div className={classes.container}>
            <Box flex={1} flexDirection={"column"} spacing={1}>
              <h3
                className={classes.title}
                style={{
                  marginTop: "0px",
                  marginBottom: "-10px"
                }}
              >
                {this.state.name}
              </h3>
              <Typography
                style={{
                  fontSize: "8px",
                  overflowWrap: "break-word",
                  fontStyle: "italic"
                }}
                className={classes.root}
              >
                <Link
                  href={`${config.floExplorer.url}/tx/${this.oipref}`}
                  target="_blank"
                  rel="noreferrer noopener"
                >
                  {this.oipref}
                </Link>
              </Typography>
              <p></p>
              <label className={classes.meta}>Description: </label>
              <p>{this.state.description}</p>
              <Box display="flex" flexDirection="row">
                <Box display="flex" flexDirection="column" width={1 / 2}>
                  <label className={classes.meta}>Steps: </label>
                  <Box p={1}>
                    {this.state.steps.map((step, key) => {
                      return (
                        <Box py={1} key={key}>
                          <Typography
                            variant="body2"
                            target="_blank"
                            rel="noreferrer noopener"
                          >
                            - {step}
                          </Typography>
                        </Box>
                      );
                    })}
                  </Box>
                </Box>
                <Box p={1} display="flex" flexDirection="column" width={1 / 2}>
                  <label className={classes.meta}>Designed by: </label>
                  <Box p={1}>
                    {this.state.by.map((people, key) => {
                      return (
                        <Link
                          variant="body2"
                          href={`${config.gateways.people}/record/${people.id}`}
                          target="_blank"
                          rel="noreferrer noopener"
                          key={key}
                          style={{ paddingRight: "5px" }}
                        >
                          {people.name}
                          {key === this.state.by.length - 1 ? "." : ", "}
                        </Link>
                      );
                    })}
                  </Box>

                  <label className={classes.meta}>Software: </label>
                  <Box p={1}>
                    {this.state.software.map((item, key) => {
                      return (
                        <Link
                          variant="body2"
                          href={`${config.gateways.software}/record/${item.id}`}
                          target="_blank"
                          rel="noreferrer noopener"
                          key={key}
                          style={{ paddingRight: "5px" }}
                        >
                          - {item.name}
                        </Link>
                      );
                    })}
                  </Box>

                  <label className={classes.meta}>Devices: </label>
                  <Box p={1}>
                    {this.state.devices.map((item, key) => {
                      return (
                        <Link
                          variant="body2"
                          href={`${config.gateways.devices}/record/${item.id}`}
                          target="_blank"
                          rel="noreferrer noopener"
                          key={key}
                          style={{ paddingRight: "5px" }}
                        >
                          - {item.name}
                        </Link>
                      );
                    })}
                  </Box>

                  <label className={classes.meta}>
                    List of Special Materials:
                  </label>
                  <Box p={1}>
                    {this.state.materials.map((item, key) => {
                      return (
                        <Link
                          variant="body2"
                          href={`${config.gateways.materials}/record/${item.id}`}
                          target="_blank"
                          rel="noreferrer noopener"
                          key={key}
                          style={{ paddingRight: "5px" }}
                        >
                          - {item.name}
                        </Link>
                      );
                    })}
                  </Box>

                  <label className={classes.meta}>Protocols used: </label>
                  <Box p={1}>
                    {this.state.protocols.map((item, key) => {
                      return (
                        <Link
                          variant="body2"
                          href={`/record/${item.id}`}
                          target="_blank"
                          rel="noreferrer noopener"
                          key={key}
                        >
                          - {item.name}
                        </Link>
                      );
                    })}
                  </Box>
                  <label className={classes.meta}>Organizations: </label>
                  <Box p={1}>
                    {this.state.organizations.map((org, key) => {
                      return (
                        <span
                          /* href={`https://org.oip.io/record/${org.id}`} */
                          target="_blank"
                          rel="noreferrer noopener"
                          key={key}
                          variant="body2"
                          style={{ paddingRight: "5px" }}
                        >
                          {org.name}
                          {key === this.state.organizations.length - 1
                            ? "."
                            : ", "}
                        </span>
                      );
                    })}
                  </Box>
                  <label className={classes.meta}>Others: </label>
                  <Box p={1}>
                    {this.state.others.map((item, key) => {
                      return (
                        <Box py={1} key={key}>
                          <Typography
                            variant="body2"
                            target="_blank"
                            rel="noreferrer noopener"
                          >
                            - {item}
                          </Typography>
                        </Box>
                      );
                    })}
                  </Box>
                  <label className={classes.meta}>Legacy Ids: </label>
                  <Box p={1}>
                    {this.state.legacyIds.map((item, key) => {
                      return (
                        <Box py={1} key={key}>
                          <Typography
                            variant="body2"
                            target="_blank"
                            rel="noreferrer noopener"
                          >
                            - {item}
                          </Typography>
                        </Box>
                      );
                    })}
                  </Box>
                </Box>
              </Box>
              <Grid item xs={12} sm={6} md={8}></Grid>
            </Box>
          </div>
        </div>
        <Footer />
      </div>
    );
  }
}

export default withStyles(recordPageStyle)(RecordPage);
